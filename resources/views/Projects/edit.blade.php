@extends('layouts.app')

@section('title')
    Tambah Project
@endsection

@section('content')

<form action="{{ route('projects.update', $project['id']) }}" method="POST">
  <div class="form-group">
    @csrf
    @method('put')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" value="{{ $project['nama'] }}">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="tanggal_mulai">Tanggal Mulai</label>
                    <input type="date" class="form-control" name="tanggal_mulai" value="{{ $project['tanggal_mulai'] }}">
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="tanggal_target">Tanggal Target</label>
                    <input type="date" class="form-control" name="tanggal_target" value="{{ $project['tanggal_target'] }}">
                </div>
            </div>
        </div>
    </div>

  <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection