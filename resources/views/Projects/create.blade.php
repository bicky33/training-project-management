@extends('layouts.app')

@section('title')
    Tambah Project
@endsection

@section('content')

<form action="{{ route('projects.store') }}" method="POST">
  <div class="form-group">
    @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" aria-describedby="emailHelp">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="tanggal_mulai">Tanggal Mulai</label>
                    <input type="date" class="form-control" name="tanggal_mulai">
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="tanggal_target">Tanggal Target</label>
                    <input type="date" class="form-control" name="tanggal_target">
                </div>
            </div>
        </div>
    </div>

  <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection