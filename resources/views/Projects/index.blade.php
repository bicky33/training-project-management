@extends('layouts.app')

@section('title')
    Project
@endsection 


@section('content')
<a href="{{ route('projects.create') }}" class="btn btn-secondary my-2">Tambah Project</a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama</th>
      <th scope="col">Tanggal Mulai</th>
      <th scope="col">Tanggal Target</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
@foreach ($projects as $project)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $project['nama'] }}</td>
        <td>{{ $project['tanggal_mulai'] }}</td>
        <td>{{ $project['tanggal_target'] }}</td>
        <td>
                <a href="{{ route('projects.show', $project['id']) }}" class="btn btn-primary">Show</a>
                <a href="{{ route('projects.edit', $project['id']) }}" class="btn btn-primary">Edit</a>
                <form action="{{ route('projects.destroy', $project['id']) }}" method="post">
                    <input type="submit" class="btn btn-danger" value="Hapus">
                    @method('delete')
                    @csrf
                </form>
        </td>
    </tr> 
@endforeach
  </tbody>
@endsection
</table>