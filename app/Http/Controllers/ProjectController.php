<?php

namespace App\Http\Controllers;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $projects = Project::all();
        return view('Projects.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi
        $validasi = $request->validate([
            'nama' => 'required|max:30',
            'tanggal_mulai' => 'required',
            'tanggal_target' => 'required',
        ]);

        Project::create ([
            'nama' => $request['nama'],
            'tanggal_mulai' => $request['tanggal_mulai'],
            'tanggal_target' => $request['tanggal_target'],
            'create_at' => now()
        ]);
        return redirect()->route('projects.index')->with(['message' => 'Projek berhasil ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $project = Project::find($id);
        return view ('Projects.edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validasi = $request->validate([
            'nama' => 'required|max:30', 
            'tanggal_mulai' => 'required',
            'tanggal_target' => 'required',
        ]);
        $project = Project::find($id);
        $project->nama = $request['nama'];
        $project->tanggal_mulai = $request['tanggal_mulai'];
        $project->tanggal_target = $request['tanggal_target'];
        $project->updated_at = now();
        $project-> save();

        return redirect()->route('projects.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $project = Project::find($id);
        $project->delete();

        return redirect()->back()->with(['message' => 'Projek berhasil dihapus!']);
    }
}
